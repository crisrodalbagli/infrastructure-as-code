variable "prefix" {
    description = "The prefix which should be used for all resources"
    default = "udacity-deploy-web-server"
}

variable "location" {
    description = "Azure Region for resources"
    default = "East US"
}

variable "resource_group_name" {
  description = "The name of the resource group in which the resources will be created"
  default     = "udacity-web-server"
}

variable "subscription_id" {
    description = "Subscription ID"
}

variable "client_id" {
    description = "Client ID"
}

variable "client_secret" {
    description = "Secret"
}

variable "virtual-net-cidr" {
    description = "Virtual Netwoek CIDR"
    default = "10.0.0.0/16"
}

variable "subnet-cidr" {
    description = "Subnet CIDR"
    default = "10.0.2.0/24"
}

variable "image_uri" {
    description = "Image ID"
    default = "/subscriptions/38b29cf2-ce90-495f-aa18-edfda3b4fe08/resourceGroups/packer/providers/Microsoft.Compute/images/ubuntuImage"
}

variable "admin" {
    description = "Admin"
    default = "admin"
}

variable "password" {
    description = "Pass"
    default = "P@ssword"
}

variable  "count" {
    description = "Count variable"
    default = "2"
}
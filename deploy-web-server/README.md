# Azure Infrastructure Operations Project: Deploying a scalable IaaS web server in Azure

### Introduction
For this project, you will write a Packer template and a Terraform template to deploy a customizable, scalable web server in Azure.

### Dependencies
1. Create an [Azure Account](https://portal.azure.com) 
2. Install the [Azure command line interface](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
3. Install [Packer](https://www.packer.io/downloads)
4. Install [Terraform](https://www.terraform.io/downloads.html)


### Overview of the Project
The overview of this project is to deploy resources to Azuire vía Terraform. One of the principle resources is to create an Image via Packer so when we deploy the virtual machine we use the Packer image created before.
The resources created on the project are the following:
- Resource_Group
- Virtual_Network
- Subnets
- Security_Groups (Inbound and Outbound Traffic)
- Network_Interfaces
- Public_Ips
- Load_Balancer
- Created_Personalize_Image
- Virtual_Machine

### Run Packer and Terraform Templates
#### Clone the repository
```bash
git clone git@bitbucket.org:crisrodalbagli/infrastructure-as-code.git
```
#### You need to create a Packer Image so you can deploy a VM with the image previously created
```bash
packer build server.json
```
#### Run the plan template from terraform to provision resources
```bash
terraform plan -out solution.plan
```
#### Run the solution plan previosly privisioned so terraform creates resources on Azure
```bash
terraform apply solution.plan
```

### Customize as you Need
- Go to `vars.tf`
- Customize as you need
## Change Prefix Name for resources
```bash
variable "prefix" {
    description = "The prefix which should be used for all resources"
    default = "Use-The-Name-You-Want-ToUse"
}
```
## Change Location for resources
```bash
variable "location" {
    description = "Azure Region for resources"
    default = "Insert-Location"
}
```
## Change Resource Group for resources
```bash
variable "resource_group_name" {
  description = "The name of the resource group in which the resources will be created"
  default     = "Use-The-Name-You-Want-ToUse"
}
```
## Change Resource Group for resources
```bash
variable "resource_group_name" {
  description = "The name of the resource group in which the resources will be created"
  default     = "Use-The-Name-You-Want-ToUse"
}
```
## Change Your Ids from Azure
```bash
variable "subscription_id" {
    description = "Subscription ID"
}

variable "client_id" {
    description = "Client ID"
}

variable "client_secret" {
    description = "Secret"
}
```
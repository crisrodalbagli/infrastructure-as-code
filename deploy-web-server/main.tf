provider "azurerm" {
    # subscription_id = var.subscription_id
    # client_id = var.client_id
    # client_secret = var.client_secret
  features {}
}

resource "azurerm_resource_group" "rg-udacity" {
  name     = "${var.prefix}-rg-project"
  location = var.location
}

resource "azurerm_virtual_network" "virtual-network" {
  name                = "${var.prefix}-network"
  address_space       = [var.virtual-net-cidr]
  location            = azurerm_resource_group.rg-udacity.location
  resource_group_name = azurerm_resource_group.rg-udacity.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.rg-udacity.name
  virtual_network_name = azurerm_virtual_network.virtual-network.name
  address_prefixes     = [var.subnet-cidr]
}

resource "azurerm_network_security_group" "nsg" {
    resource_group_name  = azurerm_resource_group.rg-udacity.name
    name = "${var.prefix}-network-nsg"
    location = azurerm_resource_group.rg-udacity.location

    security_rule {
        name = "${var.prefix}-allowVNet-Inbound"
        priority = 800
        direction = "Inbound"
        access = "Allow"
        protocol = "*"
        source_port_range = "*"
        destination_port_range = "*"
        source_address_prefix = "VNet"
        destination_address_prefix = "VNet"
    }

    security_rule {
        name = "${var.prefix}-allowVNet-Outbound"
        priority = 850
        direction = "Outbound"
        access = "Allow"
        protocol = "*"
        source_port_range = "*"
        destination_port_range = "*"
        source_address_prefix = "VNet"
        destination_address_prefix = "VNet"
    }

    security_rule {
        name                       = "${var.prefix}-All-Inbound-Internet-Traffic"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Deny"
        protocol                   = "Any"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  resource_group_name = azurerm_resource_group.rg-udacity.name
  location            = azurerm_resource_group.rg-udacity.location

  ip_configuration {
    name                          = "nic-ip"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_public_ip" "publicIP" {
  name                    = "${var.prefix}-public-ip"
  location                = azurerm_resource_group.rg-udacity.location
  resource_group_name     = azurerm_resource_group.rg-udacity.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30
}

resource "azurerm_lb" "lb" {
  name                = "${var.prefix}-lb"
  location            = azurerm_resource_group.rg-udacity.location
  resource_group_name = azurerm_resource_group.rg-udacity.name

  frontend_ip_configuration {
    name                 = "${var.prefix}-public-ip"
    public_ip_address_id = azurerm_public_ip.publicIP.id
  }
}

resource "azurerm_lb_nat_pool" "nat-pool" {
  resource_group_name            = azurerm_resource_group.rg-udacity.name
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "${var.prefix}-app-pool"
  protocol                       = "Tcp"
  frontend_port_start            = 80
  frontend_port_end              = 81
  backend_port                   = 8080
  frontend_ip_configuration_name = azurerm_public_ip.publicIP.name
}

resource "azurerm_availability_set" "vm-availability-set" {
  name                = "${var.prefix}-vm-availability-set"
  location            = azurerm_resource_group.rg-udacity.location
  resource_group_name = azurerm_resource_group.rg-udacity.name
}

# Find Image ID Created with Packer
data "azurerm_image" "search" {
  name                = "ubuntuImage"
  resource_group_name = "packer"
}

output "image_id" {
  value = "azurerm_image.search.id"
}

resource "azurerm_virtual_machine" "vm" {
  name                  = "${var.prefix}-vm"
  location              = azurerm_resource_group.rg-udacity.location
  resource_group_name   = azurerm_resource_group.rg-udacity.name
  vm_size               = "Standard_D2s_v3"
  network_interface_ids = ["${azurerm_network_interface.nic.id}"]
  count =                 var.count

  storage_image_reference {
    id = "/subscriptions/38b29cf2-ce90-495f-aa18-edfda3b4fe08/resourceGroups/packer/providers/Microsoft.Compute/images/ubuntuImage"
  }

  storage_os_disk {
    name              = "${var.prefix}-image-st"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
}

  os_profile {
    computer_name  = "${var.prefix}-comp"
    admin_username = "adminUsername"
    admin_password = "P@assword"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}



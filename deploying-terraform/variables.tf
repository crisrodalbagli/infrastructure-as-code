variable "prefix" {
    description = "The prefix which should be used for all resources"
}

variable "location" {
    description = "Azure Region for resources"
    default = "East US"
}